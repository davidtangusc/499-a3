<?php 

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/pdo.php';

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\RedirectResponse;
use ITP\Songs\SongQuery;
use Carbon\Carbon;

$session = new Session();
$session->start();

if (!$session->has('username')) {
	return (new RedirectResponse('login.php'))->send();
}

$successFlash = $session->getFlashBag()->get('success');

$songQuery = new SongQuery($pdo);
$songs = $songQuery
  ->withArtist()
  ->withGenre()
  ->orderBy('title')
  ->all();

?>

<?php include 'header.php' ?>

<div class="flash">
	<?php foreach ($successFlash as $message) : ?>
		<p>
			<?php echo $message ?>
		</p>
	<?php endforeach ?>
</div>

<div style="color: green; text-align: right;">
	<p>
		You are logged in as: <?php echo $session->get('username') ?> (<?php echo $session->get('email') ?>) | <a href="logout.php">Logout</a>
	</p>
	<p>Last Login: <?php echo Carbon::createFromTimeStamp($session->get('loggedInAt'))->diffForHumans() ?></p>
</div>

<div class="container">
	<h1>Dashboard</h1>
	<p>Welcome to the dashboard of my awesome site...</p>

	<table border="1" cellpadding="5" cellspacing="0">
		<tr>
			<th>Title</th>
			<th>Artist</th>
			<th>Genre</th>
			<th>Price</th>
		</tr>
		<?php foreach ($songs as $song) : ?>
			<tr>
				<td><?php echo $song->title ?></td>
				<td><?php echo $song->artist_name ?></td>
				<td><?php echo $song->genre ?></td>
				<td>$<?php echo $song->price ?></td>
			</tr>
		<?php endforeach ?>
	</table>
</div>

<?php include 'footer.php' ?>


