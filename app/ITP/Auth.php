<?php 

namespace ITP;

use PDO;

class Auth {

	protected $pdo;
	protected $user;

	public function __construct($pdo)
	{
		$this->pdo = $pdo;
	}

	public function attempt($username, $password)
	{
		$sql = "
			SELECT username, email 
			FROM users
			WHERE username = ? AND password = SHA1(?)
			LIMIT 1
		";

		$statement = $this->pdo->prepare($sql);
		$statement->bindParam(1, $username);
		$statement->bindParam(2, $password);
		$statement->execute();

		$user = $statement->fetch(PDO::FETCH_OBJ);
	
		if ($user) {
			$this->user = $user;
			return true;
		}

		return false;
	}

	public function getUser()
	{
		return $this->user;
	}

}