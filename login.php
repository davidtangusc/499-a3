<?php

require_once __DIR__ . '/vendor/autoload.php';

use Symfony\Component\HttpFoundation\Session\Session;

$session = new Session();
$session->start();

$errorFlashMessages = $session->getFlashBag()->get('error');

?>

<?php include 'header.php' ?>

<div class="errors">
	<?php foreach ($errorFlashMessages as $message) : ?>
		<p>
			<?php echo $message ?>
		</p>
	<?php endforeach ?>
</div>

<form action="login-process.php" method="post">
	Username: <input type="text" name="username">
	Password: <input type="password" name="password">
	<input type="submit" value="Login">
</form>

<?php include 'footer.php' ?>