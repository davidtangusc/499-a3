<?php 

require_once __DIR__ . '/pdo.php';
require_once __DIR__ . '/vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\RedirectResponse;
use ITP\Auth;

$request = Request::createFromGlobals();
$session = new Session();
$started = $session->start();

// if the user is alread logged in
if ($session->has('username')) {
	return (new RedirectResponse('dashboard.php'))->send();
}

// redirect to login page if visit this page directly
if (!$request->request->has('username') || !$request->request->has('password')) {
	return (new RedirectResponse('login.php'))->send();
}

$authentication = new Auth($pdo);

if ( $authentication->attempt($request->request->get('username'), $request->request->get('password')) ) {
	$user = $authentication->getUser();

	$session->set('username', $user->username);
	$session->set('loggedInAt', time());
	$session->set('email', $user->email);
	$session->getFlashBag()->add('success', 'You have successfully logged in!');

	(new RedirectResponse('dashboard.php'))->send();
} else {
	$session->getFlashBag()->add('error', 'Incorrect credentials.');
	(new RedirectResponse('login.php'))->send();
}